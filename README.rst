==================
ElGamal Encryption
==================


.. image:: https://img.shields.io/pypi/v/elgamal.svg
        :target: https://pypi.python.org/pypi/elgamal

.. image:: https://img.shields.io/travis/aavalos/elgamal.svg
        :target: https://travis-ci.org/aavalos/elgamal

.. image:: https://readthedocs.org/projects/elgamal/badge/?version=latest
        :target: https://elgamal.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status

.. image:: https://pyup.io/repos/github/aavalos/elgamal/shield.svg
     :target: https://pyup.io/repos/github/aavalos/elgamal/
     :alt: Updates


ElGamal implementation in Python


* Free software: GNU General Public License v3
* Documentation: https://elgamal.readthedocs.io.


To run the interactive test code, type the following in a console:

- Open a console/terminal window
- Navigate to the location where project is.
- Run the following:
    ``python elgamal_shell_test.py``
- Follow the instructions from the promt. 

Note:Keep in mind that the message (bit representation) to be encrypted/decrypted must be less than or equal than the size of the key generated in bits.

Features
--------

* TODO

Credits
---------

This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage

