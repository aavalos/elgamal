from __future__ import print_function
from elgamal import elgamal
from elgamal.elgamal import ElGamalClient, ElGamalServer
import binascii

input_func = None
try:
    input_func = raw_input
except NameError:
    input_func = input

print("ElGamal  -   by Alberto Avalos")
print("This script will encrypt/decrypt a message using "
      "ElGamal encryption system")

continue_loop = True
while continue_loop:
    bits = int(input_func('Enter the amount of bits for encryption '
                          'between 64-512: '))
    if bits < 5 or bits > 512:
        print("bits should be between 64 and 512. Restarting")
        continue
    server = ElGamalServer(bits)
    public_keys = server.get_public_keys_dict()
    print("The public keys are:")
    print(server.get_public_keys_dict())
    original_message = input_func('Enter message to encrypt as plain text. '
                                  'Note that its "int" value must be less '
                                  'than "p": ')

    # Converting string message to an int representation
    h = binascii.hexlify(original_message.encode('utf-8'))
    m = int(h, 16)
    if m > public_keys['p']:
        num_of_msg_bits = len('{0:064b}'.format(m))
        if num_of_msg_bits > bits:
            print("message is too long, it contains %s bits "
                  "and the public keys are based on %s bits. "
                  "Please increase the bits for encryption "
                  "or type a smaller message." % (num_of_msg_bits, bits))
        else:
            print("message is too long")
        print("Restarting...")
        continue
    client = ElGamalClient()
    c1, c2 = client.encrypt(public_keys['p'],
                            public_keys['g'],
                            public_keys['h'],
                            m)
    print("The encryption values are:")
    print(c1, c2)
    print("Decrypting")
    m1 = server.decrypt(c1, c2)
    if m == m1:
        print("Success!, decrypted message matches the original")
    else:
        print("Failure!, decrypted message does not match the original")

    # Converting decrypted int back to s string
    h2 = hex(m1)[2:]
    if h2.endswith('L'):
        h2 = h2[:-1]
    b2 = h2.encode('ascii')
    b3 = binascii.unhexlify(b2)
    print(b3)

    continue_response = input_func('Would you like to continue [y]: ')
    continue_loop = continue_response.lower() == 'y'
print("Goodbye!!")
