import random


def get_safe_prime(bit_size):
    p = get_random_prime(bit_size)
    q = (p - 1) // 2
    while not miller_rabin_primality_test(q, 5):
        p = get_random_prime(bit_size)
        q = (p - 1) // 2
    return p, q


def get_generator(p, factors):
    """Algorithm 4.80 from the Handbook of Applied Cryptography
    http://www.cacr.math.uwaterloo.ca/hac/about/chap4.pdf

    :param p:
    :param factors:
    :return:
    """
    b = 1
    g = None
    while b == 1:
        # 1. Choose a random element g in G.
        g = random.randrange(1, p - 1)
        # 2. For i from 1 to k do the following:
        for factor in factors:
            # 2.1 Compute b = g^(n/pi)
            r = p // factor
            b = square_and_multiply_algorithm(g, r, p)
            # 2.2 If b = 1 then go to step 1
    # 3. Return g
    return g


def get_random_prime(bit_size):
    random_number = random.getrandbits(bit_size)
    while not miller_rabin_primality_test(random_number, 5):
        random_number = random.getrandbits(bit_size)
    return random_number


def miller_rabin_primality_test(n, t):
    if n % 2 == 0 or n < 3 or t < 1:
        return False
    # Step 1: Write n-1=2^s*r such that r is odd
    prev_even = n - 1
    s = calculate_exponent(prev_even)
    r = prev_even // pow(2, s)
    # Step 2
    for x in range(0, t):
        a = random.randrange(2, n-2)
        # Step 2.2 Compute y=a^r(mod n) using S-&-M algorithm.
        y = square_and_multiply_algorithm(a, r, n)
        # Step 2.3 If y != 1 and y != n-1 do
        if y != 1 and y != prev_even:
            j = 1
            while j <= (s-1) and y != (n-1):
                # y = y ^ 2(mod n)
                y = pow(y, 2, n)
                # if y=1, we return composite
                if y == 1:
                    return False
                j = j + 1
            # if y != (n - 1), return composite
            if y != (n - 1):
                return False
    # returns likely prime
    return True


def calculate_exponent(a):
    b = a
    count = 0
    while b % 2 == 0:
        b = b // 2
        count += 1
    return count


def square_and_multiply_algorithm(a, k, m):
    # Setup: Convert k to binary
    k = '{0:b}'.format(k)[::-1]
    # Step 1: Set b=1, if k=0 return b
    b = 1
    if k == '0':
        return b
    # Step 2: x=a
    x = a
    # Step 3: if k[0] == 1 then b = a
    if k[0] == '1':
        b = a
    # Step 4: for i=1->t
    for i in range(1, len(k)):
        # Step 4.1: x=x^2(mod m)
        x = pow(x, 2, m)
        # Step 4.2 if k[i]==1 then b = x*b(mod n)
        if k[i] == '1':
            b = (x * b) % m
    # Step 5: return b
    return b
