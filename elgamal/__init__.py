# -*- coding: utf-8 -*-

"""Top-level package for ElGamal Encryption."""

__author__ = """Alberto Avalos"""
__email__ = 'aaa.avalos@outlook.com'
__version__ = '0.1.0'
