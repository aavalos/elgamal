import random

from elgamal import bigprime


class ElGamalServer:

    def __init__(self, bit_size, **kwargs):
        keys_dict = kwargs.get('keys_dict', None)
        if not keys_dict:
            keys_dict = self.__get_keys(bit_size)
        self.__public_keys = keys_dict['public']
        self.__private_key = keys_dict['private']

    def __get_keys(self, bit_size):
        p, q = bigprime.get_safe_prime(bit_size)
        g = bigprime.get_generator(p, [2, q])
        # Private key
        a = random.randrange(1, p - 2)
        h = bigprime.square_and_multiply_algorithm(g, a, p)
        return {'public': {'p': p, 'g': g, 'h': h}, 'private': a}

    def get_public_keys_dict(self):
        return self.__public_keys

    def set_public_keys_dict(self, public_keys):
        self.__public_keys = public_keys

    def get_private_key(self):
        return self.__private_key

    def set_private_key(self, private_key):
        self.__private_key = private_key

    def get_keys_dict(self):
        return {'public':  self.__public_keys, 'private': self.__private_key}

    def set_keys_dict(self, keys_dict):
        self.__public_keys = keys_dict['public']
        self.__private_key = keys_dict['private']

    def decrypt(self, c1, c2):
        exponent = self.__public_keys['p'] - 1 - self.__private_key
        c1_inverse = bigprime.square_and_multiply_algorithm(
            c1, exponent, self.__public_keys['p'])
        return (c1_inverse * c2) % self.__public_keys['p']


class ElGamalClient:

    def encrypt(self, p, g, h, m):
        if m >= p:
            raise ValueError('M value should be less than the public key')
        k = random.randrange(1, p - 2)
        c1 = bigprime.square_and_multiply_algorithm(g, k, p)
        c2 = (m * bigprime.square_and_multiply_algorithm(h, k, p)) % p
        return c1, c2
